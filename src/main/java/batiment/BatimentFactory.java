package batiment;

public class BatimentFactory {

    public static Batiment instanceBatiment(TypeBatiment type ){
        switch (type){
            case CASERNE: return new Caserne();
            default: return null;
        }
    }
}
