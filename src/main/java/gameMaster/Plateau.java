package gameMaster;

import site.Site;
import unit.Unit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Plateau {

    private static Plateau instance;
    private Map<Integer, Site> listeSite = new HashMap<>();
    private List<Unit> armees = new ArrayList<>();

    private Plateau(){};
    public static Plateau getInstance(){
        if(instance == null){
            instance = new Plateau();
        }
        return instance;
    }

    public void addSite(int siteId, int x, int y, int radius){
        listeSite.put(siteId, new Site(siteId, x, y, radius));
    }

    public Integer getNumSite(){
        return listeSite.size();
    }

    public void actualiseSite(int siteId, int structureType, int owner, int param1, int param2) {
        listeSite.get(siteId).setOwner(owner);
        listeSite.get(siteId).actualiseBatiment(structureType, param1, param2);
    }

    public void actualiserArmees(List<Unit> armeesActualise){
        this.armees = armeesActualise;
    }

    public String afficherCarte() {
        return listeSite.toString();
    }

    public String printArmees(){
        return armees.toString();
    }
}
