package unit;

public class UnitBuilder {

    public static Unit getUnit(UnitType type){
        switch (type){
            case REINE: return new Reine();
            case CHEVALIER: return new Chevalier();
            case ARCHER: return new Archer();
            default: return null;
        }

    }
}
