package site;

import batiment.Batiment;
import batiment.TypeBatiment;
import batiment.BatimentFactory;
import joueur.JoueurCamps;

public class Site {

    private int id;
    private int x;
    private int y;
    private int radius;
    private Batiment batiment;
    private JoueurCamps owner;

    public Site(int id, int x, int y, int radius) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.owner = owner;
    }


    public void setOwner(JoueurCamps owner) {
        this.owner = owner;
    }

    public void setOwner(int owner) {
        switch (owner) {
            case -1:
                this.owner = JoueurCamps.NEUTRE;
                break;
            case 0:
                this.owner = JoueurCamps.ALLIE;
                break;
            case 1:
                this.owner = JoueurCamps.ENNEMI;
                break;
        }
    }

    public void actualiseBatiment ( int structureType, int param1, int param2){
        if(isNoBatimentOrBatimentChange(structureType)){
            batiment = BatimentFactory.instanceBatiment(TypeBatiment.getType(structureType));
        }
        else {
            batiment.setParam1(param1);
            batiment.setParam2(param2);
        }


    }

    private boolean isNoBatimentOrBatimentChange (int structureType) {
        return batiment == null || batiment.getType() != TypeBatiment.getType(structureType);
    }

    @Override
    public String toString() {
        return "Site{" +
                "id=" + id +
                ", x=" + x +
                ", y=" + y +
                ", radius=" + radius +
                ", batiment=" + batiment +
                ", owner=" + owner +
                '}';
    }
}
