/*
Proudly built by org.ndx.codingame.simpleclass.Assembler on 2022-02-20T16:07:22.283+01:00[Europe/Paris]
@see https://github.com/Riduidel/codingame/tree/master/tooling/codingame-simpleclass-maven-plugin
*/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

enum TypeBatiment {

    NONE, CASERNE, MINE, TOUR;

    public static TypeBatiment getType(int structureType) {
        switch(structureType) {
            case 0:
                return MINE;
            case 1:
                return TOUR;
            case 2:
                return CASERNE;
            default:
                return NONE;
        }
    }
}

abstract class Batiment {

    protected TypeBatiment type;

    public abstract void setParam1(int param1);

    public abstract void setParam2(int param2);

    public TypeBatiment getType() {
        return type;
    }
}

class Caserne extends Batiment {

    public Caserne() {
        type = TypeBatiment.CASERNE;
    }

    @Override
    public void setParam1(int param1) {
    }

    @Override
    public void setParam2(int param2) {
    }
}

class BatimentFactory {

    public static Batiment instanceBatiment(TypeBatiment type) {
        switch(type) {
            case CASERNE:
                return new Caserne();
            default:
                return null;
        }
    }
}

enum JoueurCamps {

    NEUTRE, ALLIE, ENNEMI;

    public static JoueurCamps getCamps(int owner) {
        switch(owner) {
            case -1:
                return NEUTRE;
            case 0:
                return ALLIE;
            case 1:
                return ENNEMI;
            default:
                return null;
        }
    }
}

class Site {

    private int id;

    private int x;

    private int y;

    private int radius;

    private Batiment batiment;

    private JoueurCamps owner;

    public Site(int id, int x, int y, int radius) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.owner = owner;
    }

    public void setOwner(JoueurCamps owner) {
        this.owner = owner;
    }

    public void setOwner(int owner) {
        switch(owner) {
            case -1:
                this.owner = JoueurCamps.NEUTRE;
                break;
            case 0:
                this.owner = JoueurCamps.ALLIE;
                break;
            case 1:
                this.owner = JoueurCamps.ENNEMI;
                break;
        }
    }

    public void actualiseBatiment(int structureType, int param1, int param2) {
        if (isNoBatimentOrBatimentChange(structureType)) {
            batiment = BatimentFactory.instanceBatiment(TypeBatiment.getType(structureType));
        } else {
            batiment.setParam1(param1);
            batiment.setParam2(param2);
        }
    }

    private boolean isNoBatimentOrBatimentChange(int structureType) {
        return batiment == null || batiment.getType() != TypeBatiment.getType(structureType);
    }

    @Override
    public String toString() {
        return "Site{" + "id=" + id + ", x=" + x + ", y=" + y + ", radius=" + radius + ", batiment=" + batiment + ", owner=" + owner + '}';
    }
}

enum UnitType {

    REINE, CHEVALIER, ARCHER, GEANT;

    public static UnitType getType(Integer type) {
        switch(type) {
            case -1:
                return REINE;
            case 0:
                return CHEVALIER;
            case 1:
                return ARCHER;
            case 2:
                return GEANT;
            default:
                return null;
        }
    }
}

abstract class Unit {

    protected int x;

    protected int y;

    protected int health;

    protected UnitType type;

    protected JoueurCamps owner;

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setOwner(JoueurCamps owner) {
        this.owner = owner;
    }
}

class Plateau {

    private static Plateau instance;

    private Map<Integer, Site> listeSite = new HashMap<>();

    private List<Unit> armees = new ArrayList<>();

    private Plateau() {
    }

    ;

    public static Plateau getInstance() {
        if (instance == null) {
            instance = new Plateau();
        }
        return instance;
    }

    public void addSite(int siteId, int x, int y, int radius) {
        listeSite.put(siteId, new Site(siteId, x, y, radius));
    }

    public Integer getNumSite() {
        return listeSite.size();
    }

    public void actualiseSite(int siteId, int structureType, int owner, int param1, int param2) {
        listeSite.get(siteId).setOwner(owner);
        listeSite.get(siteId).actualiseBatiment(structureType, param1, param2);
    }

    public void actualiserArmees(List<Unit> armeesActualise) {
        this.armees = armeesActualise;
    }

    public String afficherCarte() {
        return listeSite.toString();
    }

    public String printArmees() {
        return armees.toString();
    }
}

class Archer extends Unit {
}

class Chevalier extends Unit {
}

class Reine extends Unit {
}

class UnitBuilder {

    public static Unit getUnit(UnitType type) {
        switch(type) {
            case REINE:
                return new Reine();
            case CHEVALIER:
                return new Chevalier();
            case ARCHER:
                return new Archer();
            default:
                return null;
        }
    }
}

class GameMaster {

    private static GameMaster instance = null;

    private static Integer gold = 0;

    private static boolean touchedSite;

    private GameMaster() {
    }

    public static GameMaster getInstance() {
        if (instance == null) {
            return new GameMaster();
        } else {
            return instance;
        }
    }

    public void instanciateGame(Scanner in) {
        int numSites = in.nextInt();
        for (int i = 0; i < numSites; i++) {
            int siteId = in.nextInt();
            int x = in.nextInt();
            int y = in.nextInt();
            int radius = in.nextInt();
            Plateau.getInstance().addSite(siteId, x, y, radius);
        }
    }

    public void actuliserRessources(Scanner in) {
        int ngold = in.nextInt();
        System.err.println("Gold " + ngold);
        int ntouched = in.nextInt();
        System.err.println("ntouche" + ntouched);
        setGold(gold);
        this.touchedSite = ntouched == 0;
    }

    public void actualiserCarte(Scanner in) {
        for (int i = 0; i < Plateau.getInstance().getNumSite(); i++) {
            int siteId = in.nextInt();
            // used in future leagues
            int ignore1 = in.nextInt();
            // used in future leagues
            int ignore2 = in.nextInt();
            // -1 = No structure, 2 = Barracks
            int structureType = in.nextInt();
            // -1 = No structure, 0 = Friendly, 1 = Enemy
            int owner = in.nextInt();
            int param1 = in.nextInt();
            int param2 = in.nextInt();
            Plateau.getInstance().actualiseSite(siteId, structureType, owner, param1, param2);
        }
    }

    public void actualiserArmee(Scanner in) {
        int numUnits = in.nextInt();
        List<Unit> units = new ArrayList<>();
        for (int i = 0; i < numUnits; i++) {
            int x = in.nextInt();
            int y = in.nextInt();
            int owner = in.nextInt();
            // -1 = QUEEN, 0 = KNIGHT, 1 = ARCHER
            int unitType = in.nextInt();
            int health = in.nextInt();
            Unit unit = UnitBuilder.getUnit(UnitType.getType(unitType));
            unit.setHealth(health);
            unit.setOwner(JoueurCamps.getCamps(owner));
            unit.setX(x);
            unit.setY(y);
            units.add(unit);
        }
        Plateau.getInstance().actualiserArmees(units);
    }

    private void setGold(Integer Or) {
        this.gold = Or;
    }

    public String printCarte() {
        return "Carte actuelle: " + Plateau.getInstance().afficherCarte();
    }

    public String printArmee() {
        return "Armees présentes: " + Plateau.getInstance().printArmees();
    }

    public String printRessources() {
        return "gold : " + gold + " et touchedSite : " + touchedSite;
    }
}

class Player {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        GameMaster.getInstance().instanciateGame(in);
        int tour = 0;
        while (true) {
            System.err.println("  ****  TOUR : " + tour++ + " ****  ");
            GameMaster.getInstance().actuliserRessources(in);
            System.err.println(GameMaster.getInstance().printRessources());
            GameMaster.getInstance().actualiserCarte(in);
            System.err.println(GameMaster.getInstance().printCarte());
            GameMaster.getInstance().actualiserArmee(in);
            System.err.println(GameMaster.getInstance().printArmee());
            System.out.println("BUILD ");
            System.out.println("TRAIN");
        }
    }
}
