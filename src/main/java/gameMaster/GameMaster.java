package gameMaster;

import joueur.JoueurCamps;
import unit.Unit;
import unit.UnitBuilder;
import unit.UnitType;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class GameMaster {

    private static GameMaster instance = null;
    private static Integer gold = 0;
    private static boolean touchedSite;


    private GameMaster(){}

    public static GameMaster getInstance(){
        if(instance ==null) {
            return new GameMaster();
        } else {
            return instance;
        }
    }

    public void instanciateGame(Scanner in){
        int numSites = in.nextInt();

        for (int i = 0; i < numSites; i++) {

            int siteId = in.nextInt();
            int x = in.nextInt();
            int y = in.nextInt();
            int radius = in.nextInt();

            Plateau.getInstance().addSite(siteId, x, y, radius);

        }
    }

    public void actuliserRessources(Scanner in){
        int ngold = in.nextInt();
        System.err.println("Gold " + ngold);
        int ntouched = in.nextInt();
        System.err.println("ntouche" + ntouched);
        setGold(gold);
        this.touchedSite = ntouched!= -1;
    }

    public void actualiserCarte(Scanner in){

        for (int i = 0; i < Plateau.getInstance().getNumSite(); i++) {
            int siteId = in.nextInt();
            int ignore1 = in.nextInt(); // used in future leagues
            int ignore2 = in.nextInt(); // used in future leagues
            int structureType = in.nextInt(); // -1 = No structure, 2 = Barracks
            int owner = in.nextInt(); // -1 = No structure, 0 = Friendly, 1 = Enemy
            int param1 = in.nextInt();
            int param2 = in.nextInt();

            Plateau.getInstance().actualiseSite(siteId, structureType, owner, param1, param2);
        }

    }

    public void actualiserArmee(Scanner in){
        int numUnits = in.nextInt();

        List<Unit> units = new ArrayList<>();

        for (int i = 0; i < numUnits; i++) {
            int x = in.nextInt();
            int y = in.nextInt();
            int owner = in.nextInt();
            int unitType = in.nextInt(); // -1 = QUEEN, 0 = KNIGHT, 1 = ARCHER
            int health = in.nextInt();

            Unit unit = UnitBuilder.getUnit(UnitType.getType(unitType));
            unit.setHealth(health);
            unit.setOwner(JoueurCamps.getCamps(owner));
            unit.setX(x);
            unit.setY(y);
            units.add(unit);
        }

        Plateau.getInstance().actualiserArmees(units);
    }

    private void setGold(Integer Or){
        this.gold = Or;
    }

    public String printCarte(){
        return "Carte actuelle: " + Plateau.getInstance().afficherCarte();
    }

    public String printArmee(){
        return "Armees présentes: " + Plateau.getInstance().printArmees();
    }

    public String printRessources(){
        return "gold : " + gold + " et touchedSite : " + touchedSite;
    }
}
