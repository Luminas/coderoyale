package batiment;

public enum TypeBatiment {

    NONE, CASERNE, MINE, TOUR;

    public static TypeBatiment getType(int structureType){
        switch (structureType) {
            case 0: return MINE;
            case 1: return TOUR;
            case 2: return CASERNE;
            default: return NONE;
        }
    }
}
