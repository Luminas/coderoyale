package joueur;

public enum JoueurCamps {

    NEUTRE, ALLIE, ENNEMI;

    public static JoueurCamps getCamps(int owner){
        switch (owner){
            case -1 : return NEUTRE;
            case 0 : return ALLIE;
            case 1: return ENNEMI;
            default: return null;
        }
    }
}
