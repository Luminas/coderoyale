package unit;

import joueur.JoueurCamps;

public abstract class Unit {

    protected int x;
    protected int y;
    protected int health;
    protected UnitType type;
    protected JoueurCamps owner;

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setOwner(JoueurCamps owner) {
        this.owner = owner;
    }
}
