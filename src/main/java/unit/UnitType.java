package unit;

public enum UnitType {

    REINE, CHEVALIER, ARCHER, GEANT;

    public static UnitType getType(Integer type ){
        switch (type){
            case -1: return REINE;
            case 0: return CHEVALIER;
            case 1: return ARCHER;
            case 2: return GEANT;
            default: return null;
        }
    }
}
