package batiment;

public abstract class Batiment {

    protected TypeBatiment type;

    public abstract void setParam1(int param1);
    public abstract void setParam2(int param2);

    public TypeBatiment getType() {
        return type;
    }
}
